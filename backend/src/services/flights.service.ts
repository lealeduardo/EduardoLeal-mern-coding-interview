import { FilterQuery } from 'mongoose';
import { Flight, FlightsModel, FlightStatuses } from '../models/flights.model';

export class FlightsService {
    async getAll({status, code}: Pick<Flight, 'code' | 'status'>) {
        const filters: FilterQuery<Flight> = {};


        if (status) {
            filters.status = status;
        }

        if (code) {
            filters.code = {
                $regex: `.*${code}.*`,
            };
        }

        return await FlightsModel.find(filters)
    }


    async update(code: string, status: FlightStatuses) {
        return await FlightsModel.updateOne({code}, {status});
    }
}
