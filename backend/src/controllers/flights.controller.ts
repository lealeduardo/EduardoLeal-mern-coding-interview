import { IsEnum, IsOptional, IsString } from 'class-validator';
import { JsonController, Get, Put, Param, Body, QueryParam, QueryParams } from 'routing-controllers'
import { FlightStatuses } from '../models/flights.model';
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService();


class UpdateFlightDTO {
    @IsEnum(FlightStatuses)
    status: FlightStatuses;
}


class GetFlightDTO {
    @IsOptional()
    @IsEnum(FlightStatuses)
    status: FlightStatuses;

    @IsOptional()
    @IsString()
    code: string;
}


@JsonController('/flights', { transformResponse: false })
export default class FlightsController {


    
    @Put('/:code')
    async update(@Param('code') code: string, @Body({required: true}) {status}: UpdateFlightDTO) {
        console.log(`updating flight ${code}`, {code, status});
        try {
            await flightsService.update(code, status);

            return {
                status: 200,
            };
        } catch (error) {
            console.error(error);
            return {
                status: 400,
                message: (error instanceof Error) ? error.message : undefined,
            }
        }
        
    }


    @Get('')
    async getAll(@QueryParams() query: GetFlightDTO) {
        return {
            status: 200,
            data: await flightsService.getAll(query),
        }
    }
}
