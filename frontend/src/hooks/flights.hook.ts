import { useMutation, useQuery, useQueryClient } from "react-query";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export interface MutationError {
  response: {
    data: {
      message: string;
    };
  };
}

const flightsKeys = {
  all: ['flights'],
}

export function useFlights() {
  const query = useQuery(flightsKeys.all, () => backendClient.getFlights());

  return query?.data?.data;
}


export function useFlightsMutation() {
  const queryClient = useQueryClient();

  return useMutation(backendClient.updateFlights, {
    onSuccess: () => {
      queryClient.invalidateQueries(flightsKeys.all);
    }
  })
}