import React, { useCallback, useState } from "react";
import { RouteComponentProps } from "@reach/router";
import { Container, Box, Typography } from "@material-ui/core";

import { FlightCard } from "../components/flights/flight-card";
import { FlightModel, FlightStatuses } from "../models/flight.model";

import { useFlights, useFlightsMutation } from "../hooks/flights.hook";

export const FlightsPage: React.FC<RouteComponentProps> = () => {
  const flights = useFlights();

  const flightMutation = useFlightsMutation();

  const onFlightUpdate = useCallback(async (code: string, status: FlightStatuses) => {
    try {
      await flightMutation.mutateAsync({code, status});
    } catch (error) {
      // TODO: implement erro handling
      console.error(error);
    }
  }, []);

  return (
    <Box>
      <Box>
        <Typography
          style={{ marginLeft: "15px", marginTop: "15px" }}
          variant="h4"
        >
          Scheduled Flights
        </Typography>
      </Box>

      <Box>
        {flights?.length
          ? flights.map((flight: FlightModel) => (
              <FlightCard
                key={flight.code}
                onFlightUpdate={onFlightUpdate}
                origin={flight.origin}
                destination={flight.destination}
                code={flight.code}
                status={flight.status}
              />
            ))
          : null}
      </Box>
    </Box>
  );
};
